This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Resource Registry Context Client

## [v4.2.0-SNAPSHOT]

- Added support for paginated results [#24648]
- Removed usage of shared context cache


## [v4.1.1]

- Migrated code to reorganized E/R format [#24992]


## [v4.1.0]

- Enhanced gcube-bom version
- Added usage of common-utility to overcome issues with different Smartgears version (i.e. 3 and 4)
- Added the possibility for a client to add additional HTTP headers
- Added the possibility to create a client instance by specifying context


## [v4.0.1]

- Uniformed raised exception [#21993]
- Client gets service URL using resource-registry-api lib utility [#23658]


## [v4.0.0]

- Used ContextCache to make the client more efficient
- Switched JSON management to gcube-jackson [#19116]


## [v3.0.0] [r4.21.0] - 2020-03-30

- Refactored code to support IS Model reorganization (e.g naming, packages)
- Using gxREST in place of custom class of resource-regsitry-api [#11455]
- Refactored code to support renaming of Embedded class to Property [#13274]


## [v2.0.0] [r4.13.0] - 2018-11-20

- Using new resource-registry REST interface [#11905]


## [v1.0.0] [r4.9.0] - 2017-12-20

- First Release [#10247]

