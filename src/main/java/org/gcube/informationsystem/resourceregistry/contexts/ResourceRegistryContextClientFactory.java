package org.gcube.informationsystem.resourceregistry.contexts;

import org.gcube.informationsystem.resourceregistry.api.Constants;
import org.gcube.informationsystem.resourceregistry.api.rest.ServiceInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ResourceRegistryContextClientFactory {
	
	private static final Logger logger = LoggerFactory.getLogger(ResourceRegistryContextClientFactory.class);
	
	public static String getResourceRegistryURL() {
		String address = String.format("%s/%s", ServiceInstance.getServiceURL(),Constants.SERVICE_NAME);
		return address;
	}
	
	public static String getResourceRegistryURL(String context) {
		String address = String.format("%s/%s", ServiceInstance.getServiceURL(context),Constants.SERVICE_NAME);
		return address;
	}
	
	public static ResourceRegistryContextClient create() {
		String address = getResourceRegistryURL();
		logger.trace("The {} will be contacted at {}", Constants.SERVICE_NAME, address);
		return new ResourceRegistryContextClientImpl(address);
	}
	
	public static ResourceRegistryContextClient create(String context) {
		String address = getResourceRegistryURL(context);
		logger.trace("The {} will be contacted at {}", Constants.SERVICE_NAME, address);
		return new ResourceRegistryContextClientImpl(address);
	}
	
}
