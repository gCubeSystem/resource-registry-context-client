package org.gcube.informationsystem.resourceregistry.contexts;

import java.util.List;
import java.util.UUID;

import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.resourceregistry.api.contexts.ContextCache;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.contexts.ContextAlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.contexts.ContextNotFoundException;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface ResourceRegistryContextClient {

	public void addHeader(String name, String value);
	
	public ContextCache getContextCache();
	
	public List<Context> all() throws ResourceRegistryException;
	
	public Context create(Context context) throws ContextAlreadyPresentException, ResourceRegistryException;

	public String create(String context) throws ContextAlreadyPresentException, ResourceRegistryException;

	public Context read(Context context) throws ContextNotFoundException, ResourceRegistryException;
	
	public boolean exist(UUID uuid) throws ResourceRegistryException;

	public boolean exist(String uuid) throws ResourceRegistryException;

	public Context read(UUID uuid) throws ContextNotFoundException, ResourceRegistryException;

	public String read(String uuid) throws ContextNotFoundException, ResourceRegistryException;

	public Context readCurrentContext() throws ContextNotFoundException, ResourceRegistryException;

	public Context update(Context context) throws ContextNotFoundException, ResourceRegistryException;

	public String update(String context) throws ContextNotFoundException, ResourceRegistryException;

	public boolean delete(Context context) throws ContextNotFoundException, ResourceRegistryException;

	public boolean delete(UUID uuid) throws ContextNotFoundException, ResourceRegistryException;

	public boolean delete(String uuid) throws ContextNotFoundException, ResourceRegistryException;

//	public Context changeState(Context context) throws ContextNotFoundException, ResourceRegistryException;
//	
//	public String changeState(String context) throws ContextNotFoundException, ResourceRegistryException;
	
}
