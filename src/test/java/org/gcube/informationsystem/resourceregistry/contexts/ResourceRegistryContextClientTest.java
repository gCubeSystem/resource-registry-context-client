package org.gcube.informationsystem.resourceregistry.contexts;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.core.JsonProcessingException;
import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.contexts.impl.entities.ContextImpl;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.contexts.reference.relations.IsParentOf;
import org.gcube.informationsystem.resourceregistry.api.contexts.ContextCache;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.contexts.ContextAlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.contexts.ContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.contexts.ContextNotFoundException;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResourceRegistryContextClientTest extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(ResourceRegistryContextClientTest.class);
	
	protected ResourceRegistryContextClient resourceRegistryContextClient;
	
	public ResourceRegistryContextClientTest() {
		if(ContextTest.RESOURCE_REGISTRY_URL !=null  && !ContextTest.RESOURCE_REGISTRY_URL.isEmpty()) {
			resourceRegistryContextClient = new ResourceRegistryContextClientImpl(ContextTest.RESOURCE_REGISTRY_URL);
		}else {
			resourceRegistryContextClient = ResourceRegistryContextClientFactory.create();
		}
	}
	
	public static final String CTX_NAME_A = "A";
	public static final String CTX_NAME_B = "B";
	public static final String CTX_NAME_C = "C";
	
	public static void checkUUIDAndMetadata(IdentifiableElement er, UUID uuid, boolean create) {
		Assert.assertTrue(er.getMetadata() != null);
		Assert.assertTrue(er.getID() != null);
		
		if(uuid != null) {
			Assert.assertTrue(er.getID().compareTo(uuid) == 0);
		}
		
		String user = getUser();
		Assert.assertTrue(er.getMetadata().getLastUpdateBy().compareTo(user) == 0);
		
		if(create) {
			Assert.assertTrue(er.getMetadata().getCreatedBy().compareTo(user) == 0);
			Assert.assertTrue(er.getMetadata().getCreationTime().compareTo(er.getMetadata().getLastUpdateTime()) == 0);
		} else {
			Assert.assertTrue(er.getMetadata().getCreationTime().before(er.getMetadata().getLastUpdateTime()));
		}
		
	}
	
	protected void assertions(Context pre, Context post, boolean checkParent, boolean create) {
		if(checkParent) {
			if(pre.getMetadata() != null) {
				checkUUIDAndMetadata(post, pre.getID(), create);
			} else {
				checkUUIDAndMetadata(post, null, create);
			}
		}
		
		Assert.assertTrue(pre.getName().compareTo(post.getName()) == 0);
		if(checkParent && pre.getParent() != null && post.getParent() != null) {
			Context preParent = pre.getParent().getSource();
			Context postParent = post.getParent().getSource();
			assertions(preParent, postParent, false, false);
		}
	}
	
	protected Context read(UUID uuid) throws ResourceRegistryException, IOException {
		Context c = resourceRegistryContextClient.read(uuid);
		Assert.assertTrue(c.getMetadata() != null);
		Assert.assertTrue(c.getID() != null);
		Assert.assertTrue(c.getID().compareTo(uuid) == 0);
		return c;
	}
	
	protected Context create(Context context) throws ResourceRegistryException, IOException {
		Context c = resourceRegistryContextClient.create(context);
		assertions(context, c, true, true);
		return c;
	}
	
	protected Context update(Context context) throws ResourceRegistryException, IOException {
		Context c = resourceRegistryContextClient.update(context);
		assertions(context, c, true, false);
		return c;
	}
	
	protected boolean delete(UUID uuid) throws ResourceRegistryException {
		boolean deleted = resourceRegistryContextClient.delete(uuid);
		Assert.assertTrue(deleted);
		logger.debug("Deleted {} with UUID {}", Context.NAME, uuid);
		return deleted;
	}
	
	protected boolean delete(Context context) throws ResourceRegistryException {
		return delete(context.getID());
	}
	
	protected void invalidCreate(Context context) throws ResourceRegistryException, IOException {
		try {
			Context c = create(context);
			throw new RuntimeException(ElementMapper.marshal(c) + " was created successfully. This is not what we expected");
		} catch(ContextAlreadyPresentException e) {
			logger.debug("As expected {} cannot be created.", ElementMapper.marshal(context));
		}
	}
	
	protected void invalidUpdate(Context context) throws ResourceRegistryException, IOException {
		try {
			Context c = update(context);
			throw new RuntimeException(ElementMapper.marshal(c) + " was updated successfully. This is not what we expected");
		} catch(ContextAlreadyPresentException e) {
			logger.debug("As expected {} cannot be updated.", ElementMapper.marshal(context));
		}
	}
	
	protected void invalidDelete(Context context) throws ResourceRegistryException, JsonProcessingException {
		String contextString = ElementMapper.marshal(context);
		try {
			delete(context);
			throw new RuntimeException(contextString + " was deleted successfully. This is not what we expected");
		} catch(ContextException e) {
			logger.debug("As expected {} cannot be deleted.", contextString);
		}
	}
	
	@Test
	public void readCurrentContext() throws Exception {
		Context cc = resourceRegistryContextClient.readCurrentContext();
		logger.debug(ElementMapper.marshal(cc));
	}
	
	// @Test
	public void readTest() throws Exception {
		UUID uuid = UUID.fromString("4828d488-285b-4383-af4b-4d72069ad11b");
		Context gcube = read(uuid);
		logger.debug(ElementMapper.marshal(gcube));
	}
	
	@Test
	public void completeTest() throws Exception {
		Context contextA1 = new ContextImpl(CTX_NAME_A);
		contextA1 = create(contextA1);
		// ________A1________
		
		Context contextA2 = new ContextImpl(CTX_NAME_A);
		contextA2.setParent(contextA1);
		contextA2 = create(contextA2);
		// ________A1________
		// ___A2
		
		Context contextB3 = new ContextImpl(CTX_NAME_B);
		contextB3.setParent(contextA2);
		contextB3 = create(contextB3);
		// ________A1________
		// ___A2
		// B3
		
		Context contextB4 = new ContextImpl(CTX_NAME_B);
		contextB4.setParent(contextA1);
		contextB4 = create(contextB4);
		// ________A1________
		// ___A2_______B4____
		// B3
		
		Context contextA5 = new ContextImpl(CTX_NAME_A);
		contextA5.setParent(contextB4);
		contextA5 = create(contextA5);
		// ________A1________
		// ___A2_______B4____
		// B3______________A5
		
		Context invalidContextA1 = new ContextImpl(CTX_NAME_A);
		invalidCreate(invalidContextA1);
		
		Context invalidContextA2 = new ContextImpl(CTX_NAME_A);
		invalidContextA2.setParent(contextA1);
		invalidCreate(invalidContextA2);
		
		Context invalidContextB3 = new ContextImpl(CTX_NAME_B);
		invalidContextB3.setParent(contextA2);
		invalidCreate(invalidContextB3);
		
		Context invalidContextB4 = new ContextImpl(CTX_NAME_B);
		invalidContextB4.setParent(contextA1);
		invalidCreate(invalidContextB4);
		
		Context invalidContextA5 = new ContextImpl(CTX_NAME_A);
		invalidContextA5.setParent(contextB4);
		invalidCreate(invalidContextA5); // Trying to recreate A5. Fails
		
		// Trying to move A5 as child of A1. It fails due to A2.
		Context nullContext = null;
		contextA5.setParent(nullContext);
		invalidUpdate(contextA5);
		contextA5.setParent(contextB4);
		// ________A1________
		// ___A2_______B4____
		// B3______________A5
		
		nullContext = null;
		contextB4.setParent(nullContext);
		update(contextB4);
		// _____A1____B4_____
		// __A2__________A5__
		// B3
		
		contextB4.setParent(contextA1);
		update(contextB4);
		// ________A1________
		// ___A2_______B4____
		// B3______________A5
		
		// Trying to rename with the new name A. It fails due to A5.
		contextB3.setName(CTX_NAME_A);
		update(contextB3);
		// ________A1________
		// ___A2_______B4____
		// A3______________A5
		
		// After Restoring name B, trying to move B3 as child of A1. It fails due to B4.
		contextB3.setName(CTX_NAME_B);
		contextB3.setParent(contextA1);
		invalidUpdate(contextB3);
		// ________A1________
		// ___A2_______B4____
		// A3______________A5
		
		// Restoring A3 (was B3) as B3 and with parent A2.OK.
		contextB3.setName(CTX_NAME_B);
		contextB3.setParent(contextA2);
		update(contextB3);
		// ________A1________
		// ___A2_______B4____
		// B3______________A5
		
		// This update should not has eny effects except updating the lastUpdateTime.
		contextB3.setName(CTX_NAME_B);
		contextB3.setParent(contextA2);
		update(contextB3);
		
		// Trying to move A5 as child of A1. It fails due to A2.
		contextA5.setParent(contextA1);
		invalidUpdate(contextA5);
		// Restoring A5
		contextA5.setParent(contextB4);
		// ________A1________
		// ___A2_______B4____
		// B3______________A5
		
		// Moving B3 as child of B4. OK.
		contextB3.setParent(contextB4);
		update(contextB3);
		// ________A1________
		// ___A2_______B4____
		// ________B3______A5
		
		// Restoring the initial situation by moving B3 as child of A2. OK.
		contextB3.setParent(contextA2);
		update(contextB3);
		// ________A1________
		// ___A2_______B4____
		// B3______________A5
		
		// Renaming B3 as C3. OK.
		contextB3.setName(CTX_NAME_C);
		update(contextB3);
		// ________A1________
		// ___A2_______B4____
		// C3______________A5
		
		// Moving C3 (was B3) as child of A1. Now it is possible. OK.
		contextB3.setParent(contextA1);
		update(contextB3);
		// ________A1________
		// ___A2___C3___B4___
		// ________________A5
		
		// Trying to rename C3 (was B3) newly to B3. Fails due to B4.
		contextB3.setName(CTX_NAME_B);
		invalidUpdate(contextB3);
		// ________A1________
		// ___A2___C3___B4___
		// ________________A5
		
		// Moving back C3 (was B3) as child of A2. OK.
		contextB3.setParent(contextA2);
		update(contextB3);
		// ________A1________
		// ___A2_______B4____
		// C3______________A5
		
		// Renaming C3 (was B3) to B3. OK.
		contextB3.setName(CTX_NAME_B);
		update(contextB3);
		// ________A1________
		// ___A2_______B4____
		// B3______________A5
		
		// The following delete are not allowed because they are not child contexts
		invalidDelete(contextA1);
		invalidDelete(contextA2);
		invalidDelete(contextB4);
		
		delete(contextA5);
		// ________A1________
		// ___A2_______B4____
		// B3
		
		try {
			delete(contextA5);
		} catch(ContextNotFoundException e) {
			logger.debug("The context with uuid {} was not found. (Was already deleted)",
					contextA5.getID());
		}
		
		delete(contextB3);
		// ________A1________
		// ___A2_______B4____
		
		delete(contextB4);
		// ________A1________
		// ___A2
		
		delete(contextA2);
		// ________A1________
		
		delete(contextA1);
		logger.debug("The DB should be now clean");
	}
	
	@Test
	public void testGetAll() throws Exception {
		List<Context> all = resourceRegistryContextClient.all();
		for(Context c : all) {
			logger.debug("{}", ElementMapper.marshal(c));
		}
	}
	
	// @Test
	public void createDissertationContext() throws Exception {
		Context d4science = new ContextImpl("d4science");
		d4science = create(d4science);
		
		Context soBigData = new ContextImpl("SoBigData");
		soBigData.setParent(d4science);
		soBigData = create(soBigData);
		
		Context tagMe = new ContextImpl("TagMe");
		tagMe.setParent(soBigData);
		tagMe = create(tagMe);
		
		Context blueBRIDGE = new ContextImpl("BlueBRIDGE");
		blueBRIDGE.setParent(d4science);
		blueBRIDGE = create(blueBRIDGE);
		
		Context biodiversityLab = new ContextImpl("Biodiversity Lab");
		biodiversityLab.setParent(blueBRIDGE);
		biodiversityLab = create(biodiversityLab);
		
	}
	
	// @Test
	public void createDevContext() throws Exception {
		Context gcube = new ContextImpl("gcube");
		gcube = create(gcube);
		
		Context devsec = new ContextImpl("devsec");
		devsec.setParent(gcube);
		devsec = create(devsec);
		
		Context devVRE = new ContextImpl("devVRE");
		devVRE.setParent(devsec);
		devVRE = create(devVRE);
		
		Context devNext = new ContextImpl("devNext");
		devNext.setParent(gcube);
		devNext = create(devNext);
		
		Context nextNext = new ContextImpl("NextNext");
		nextNext.setParent(devNext);
		nextNext = create(nextNext);
		
		Context preprod = new ContextImpl("preprod");
		preprod.setParent(gcube);
		preprod = create(preprod);
		
		Context preVRE = new ContextImpl("preVRE");
		preVRE.setParent(preprod);
		preVRE = create(preVRE);
	}
	
	// @Test
	public void createPARTHENOSContext() throws Exception {
		// /d4science.research-infrastructures.eu/ParthenosVO/PARTHENOS_Registry
		
		Context d4science = new ContextImpl("d4science.research-infrastructures.eu");
		create(d4science);
		
		Context parthenosVO = new ContextImpl("ParthenosVO");
		parthenosVO.setParent(d4science);
		create(parthenosVO);
		
		Context parthenosRegistry = new ContextImpl("PARTHENOS_Registry");
		parthenosRegistry.setParent(parthenosVO);
		create(parthenosRegistry);
		
	}
	
	// @Test
	public void createMissingContext() throws Exception {
		UUID devNextUUID = UUID.fromString("");
		
		Map<String,UUID> contexts = new HashMap<>();
		contexts.put("", devNextUUID);
		
		for(String contextName : contexts.keySet()) {
			Context parent = read(contexts.get(contextName));
			Context context = new ContextImpl(contextName);
			context.setParent(parent);
			create(context);
		}
		
	}
	
	
	@Test
	public void testGetAllContexts() throws Exception {
		List<Context> contexts = resourceRegistryContextClient.all();
		logger.debug("{}", contexts);
		
		ContextCache contextCache = ContextCache.getInstance();
		Map<UUID, String> uuidToContextFullName = contextCache.getUUIDToContextFullNameAssociation();
		logger.debug("{}", uuidToContextFullName);

		
		for(Context c : contexts) {
			UUID uuid = c.getID();
			if(c.getParent()!=null) {
				IsParentOf isParentOf = c.getParent();
				Context parentContext = isParentOf.getSource();
				UUID parentUUID = parentContext.getID();
				Assert.assertEquals(parentContext, contextCache.getContextByUUID(parentUUID));
				List<IsParentOf> children = parentContext.getChildren();
				boolean found = false;
				for(IsParentOf ipo : children) {
					if(ipo.equals(isParentOf)) {
						found = true;
						break;
					}
				}
				Assert.assertTrue(found);
				logger.debug("{} : {} (parent {} : {})", c.getID(), contextCache.getContextFullNameByUUID(uuid), parentUUID, contextCache.getContextFullNameByUUID(parentUUID));
			}else {
				logger.debug("{} : {}", c.getID(), contextCache.getContextFullNameByUUID(uuid));
			}
		}
		
		Context currentContext = resourceRegistryContextClient.readCurrentContext();
		logger.debug("Current context : {}", currentContext);
		
		for(Context c : contexts) {
			UUID uuid = c.getID();
			Context context = resourceRegistryContextClient.read(uuid);
			String fullName = ContextCache.getInstance().getContextFullNameByUUID(uuid);
			logger.debug("{} - {} : {}", uuid, fullName, context);
		}
	}
}
